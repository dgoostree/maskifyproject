package com.Goostree.MaskifyProject;

public class Maskify {
	private static final int UNMASKED_LENGTH = 4;
	
	public static String maskify(String str) {
		
		if(null != str) {
			char[] strArray = str.toCharArray();
			
			if(str.length() > UNMASKED_LENGTH) {	
				for(int i = 0; i< str.length(); i++)
				{
					if( i < str.length() - UNMASKED_LENGTH ) {
						strArray[i] = '#';
					}
				}
			}
					
			str = String.valueOf(strArray);
		}
		
		return str;
	}
}
