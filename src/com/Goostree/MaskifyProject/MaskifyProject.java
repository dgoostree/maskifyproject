package com.Goostree.MaskifyProject;

public class MaskifyProject {

	public static void main(String[] args) {
		System.out.println(Maskify.maskify("123"));
		System.out.println(Maskify.maskify("1234"));
		System.out.println(Maskify.maskify("12345"));
		System.out.println(Maskify.maskify("123456"));
		
		System.out.println(Maskify.maskify(null));
	}
}
